<?php

namespace App\Http\Controllers;

use App\Action\CompareListing\AddComparisonAction;
use App\Action\CompareListing\RemoveComparisonAction;
use App\Http\Requests\StoreCompareListingRequest;
use App\Models\CompareListing;
use Illuminate\Support\Facades\Auth;

class CompareListingsController extends Controller
{
    public function addComparison(StoreCompareListingRequest $request)
    {
        $action = new AddComparisonAction();
        $data = $action->execute($request);
        return $data;
    }

    public function removeComparison(StoreCompareListingRequest $request)
    {
        $action = new RemoveComparisonAction();
        $data = $action->execute($request);
        return $data;
    }
}