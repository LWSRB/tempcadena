<?php

namespace App\Http\Controllers;

use App\Action\Wishlist\AddWishlistAction;
use App\Action\Wishlist\GetWishlistAction;
use App\Action\Wishlist\RemoveWishlistAction;
use App\Http\Requests\StoreWishlistRequest;
use App\Models\Wishlist;
use Illuminate\Http\Request;

class WishlistsController extends Controller
{
    public function addWishlist(StoreWishlistRequest $request)
    {
        $action = new AddWishlistAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getWishlist(Request $request)
    {
        $action = new GetWishlistAction();
        $data = $action->execute($request);
        return $data;
    }

    public function removeWishlist($data)
    {
        $action = new RemoveWishlistAction;
        $data = $action->execute($data);
        return $data;
    }
}
