<?php

namespace App\Http\Controllers;

use App\Action\RegisteredDistributor\ApproveByDealerAction;
use App\Action\RegisteredDistributor\GetRegisteredDistributorAction;
use App\Action\RegisteredDistributor\RemoveRegisteredDistributorAction;
use App\Action\RegisteredDistributor\RequestRegistrationByDistributorAction;
use App\Models\RegisteredDistributor;
use Illuminate\Http\Request;

class RegisteredDistributorsController extends Controller
{
    public function requestAddDistributor(Request $request)
    {
        $action = new RequestRegistrationByDistributorAction();
        $data = $action->execute($request);
        return $data;
    }

    public function removeRegisteredDistributor($id)
    {
        $action = new RemoveRegisteredDistributorAction();
        $data = $action->execute($id);
        return $data;
    }

    public function getRegisteredDistributors()
    {
        $action = new GetRegisteredDistributorAction();
        $data = $action->execute();
        return $data;
    }

    public function approveDistributorRequest($id, Request $request)
    {
        $action = new ApproveByDealerAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}