<?php

namespace App\Http\Controllers;

use App\Action\User\ChangePasswordAction;
use App\Action\User\GetDealersListAction;
use App\Action\User\GetDistributorsListAction;
use App\Action\User\UpdateUserDetailsAction;
use App\Action\User\UpdateUserRoleAction;
use App\Action\User\UserLoginAction;
use App\Action\User\UserRegistrationAction;
use App\Action\User\ProviderLoginOrRegistrationAction;
use App\Action\User\FacebookLoginAction;
use App\Http\Requests\RegisterNewUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    protected $providers = [
        'facebook', 'google'
    ];

    public function RegisterUser(RegisterNewUserRequest $request)
    {
        $action = new UserRegistrationAction();
        $data = $action->execute($request);
        return $data;
    }

    public function UserLogin(RegisterNewUserRequest $request)
    {
        $action = new UserLoginAction();
        $data = $action->execute($request);
        return $data;
    }

    public function UserDetails()
    {
        return response()->json(Auth::user(), 200);
    }

    public function UserLogout()
    {
        Auth::logout();
        return response()->json('Logout Successful', 200);
    }

    public function UpdateUserRole($id, Request $request)
    {
        $action = new UpdateUserRoleAction();
        $data = $action->execute($id, $request);
        return $data;
    }

    public function GetDealersList(Request $request)
    {
        $action = new GetDealersListAction();
        $data = $action->execute($request);
        return $data;
    }

    public function GetDistributorsList(Request $request)
    {
        $action = new GetDistributorsListAction();
        $data = $action->execute($request);
        return $data;
    }

    public function EditUserDetails($id, Request $request)
    {
        $action = new UpdateUserDetailsAction();
        $data = $action->execute($id, $request);
        return $data;
    }

    public function ProviderLogin($request)
    {
        $action = new ProviderLoginOrRegistrationAction();
        $data = $action->execute($request);
        return $data;
    }

    public function ChangePassword($id, Request $request)
    {
        $action = new ChangePasswordAction();
        $record = $action->execute($id, $request);
        return $record;
    }

    public function FacebookLogin()
    {   
        return Socialite::driver('facebook')->redirect();
    }

    public function FacebookRedirect()
    {
        $action = new FacebookLoginAction();
        $record = $action->execute();
        return $record;
    }

    public function GoogleLogin()
    {
        return Socialite::driver('google')->redirect();
    }

    public function GoogleRedirect()
    {

    }
}
