<?php

namespace App\Http\Controllers;

use App\Action\Listing\ActivateListingAction;
use App\Action\Listing\ApproveListingAction;
use App\Action\Listing\CreateListingAction;
use App\Action\Listing\DeleteListingAction;
use App\Action\Listing\GetListingAction;
use App\Action\Listing\PublicListingAction;
use App\Action\Listing\SearchAction;
use App\Action\Listing\SearchListingAction;
use App\Action\Listing\UpdateListingAction;
use App\Http\Requests\StoreListingRequest;
use App\Http\Requests\UpdateListingRequest;
use App\Models\Listing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ListingsController extends Controller
{
    public function newListing(StoreListingRequest $request)
    {
        $action = new CreateListingAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getAllListing(Request $request)
    {
        $action = new GetListingAction();
        $data = $action->execute($request);
        return $data;
    }

    public function searchListing($id)
    {
        $action = new SearchListingAction();
        $record = $action->execute($id);
        return $record;
    }

    public function editListing(UpdateListingRequest $request, $id)
    {
        $action = new UpdateListingAction();
        $data = $action->execute($request, $id);
        return $data;
    }


    public function deleteListing($id)
    {
        $action = new DeleteListingAction();
        $data = $action->execute($id);
        return $data;
    }

    public function search(Request $request)
    {
        $action = new SearchAction();
        $data = $action->execute($request);
        return $data;
    }

    public function approve($id, Request $request)
    {
        $action = new ApproveListingAction();
        $data = $action->execute($id, $request);
        return $data;
    }

    public function activate($id, Request $request)
    {
        $action = new ActivateListingAction();
        $data = $action->execute($id, $request);
        return $data;
    }

    public function changePrivacy($id, Request $request)
    {
        $action = new PublicListingAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}
