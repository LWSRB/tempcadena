<?php

namespace App\Http\Controllers;

use App\Action\Brand\ApproveBrandAction;
use App\Action\Brand\CreateBrandAction;
use App\Action\Brand\DeleteBrandAction;
use App\Action\Brand\GetAllBrandAction;
use App\Action\Brand\SearchBrandAction;
use App\Action\Brand\UpdateBrandAction;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrandsController extends Controller
{
    public function newBrand(Request $request)
    {
        $action = new CreateBrandAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getBrand(Request $request)
    {
        $action = new GetAllBrandAction();
        $data = $action->execute($request);
        return $data;
    }

    public function searchBrand($id, Request $request)
    {
        $action = new SearchBrandAction();
        $record = $action->execute($id, $request);
        return $record;
    }

    //working
    public function editBrand(Request $request, $id)
    {
        $action = new UpdateBrandAction();
        $data = $action->execute($request, $id);
        return $data;
    }

    public function deleteBrand($id)
    {
        $action = new DeleteBrandAction();
        $data = $action->execute($id);
        return $data;
    }

    public function approveBrand($id, Request $request)
    {
        $action = new ApproveBrandAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}
