<?php

namespace App\Http\Controllers;

use App\Action\CarouselImage\CreateCarouselImageAction;
use App\Action\CarouselImage\DeleteImageAction;
use App\Action\CarouselImage\EditCarouselAction;
use App\Models\CarouselImage;
use Illuminate\Http\Request;

class CarouselImagesController extends Controller
{

    public function newCarouselImage(Request $request)
    {
        $action = new CreateCarouselImageAction();
        $data = $action->execute($request);
        return $data;
    }


    public function getAllCarouselImages()
    {
        if (CarouselImage::count() == 0)
            return response()->json(null, 204);
        else
            return response()->json(CarouselImage::all(), 200);
    }


    public function searchCarouselImage($id)
    {
        $record = CarouselImage::whereId($id)->get();
        if ($record == null)
            return response()->json(null, 204);
        else
            return response()->json($record, 200);
    }

    public function editCarouselImage(Request $request, $id)
    {
        $action = new EditCarouselAction();
        $data = $action->execute($request, $id);
        return $data;
    }


    public function deleteCarouselImage($id)
    {
        $action = new DeleteImageAction();
        $data = $action->execute($id);
        return $data;
    }
}