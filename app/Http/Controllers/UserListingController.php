<?php

namespace App\Http\Controllers;

use App\Action\UserListing\CreateUserListingAction;
use App\Action\UserListing\DeleteUserListingAction;
use App\Action\UserListing\GetAllPromoAction;
use App\Action\UserListing\SearchUserListingAction;
use App\Action\UserListing\UpdateUserListingAction;
use App\Action\UserListing\UserListingActivationAction;
use App\Http\Requests\UpdateUserListingRequest;
use App\Models\UserListing;
use Illuminate\Http\Request;

class UserListingController extends Controller
{
    public function newUserListing(Request $request)
    {
        $action = new CreateUserListingAction();
        $data = $action->execute($request);
        return $data;
    }

    public function editUserListing(UpdateUserListingRequest $request, $id)
    {
        $action = new UpdateUserListingAction();
        $data = $action->execute($request, $id);
        return $data;
    }

    public function getAllUserListing(Request $request)
    {
        $action = new GetAllPromoAction();
        $data = $action->execute($request);
        return $data;
    }

    public function searchUserListing($id, Request $request)
    {
        $action = new SearchUserListingAction();
        $record = $action->execute($id, $request);
        return $record;
    }

    public function deleteUserListing($id)
    {
        $action = new DeleteUserListingAction();
        $data = $action->execute($id);
        return $data;
    }

    public function userListingActivation($id, Request $request)
    {
        $action = new UserListingActivationAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}
