<?php

namespace App\Http\Controllers;

use App\Action\DealerListing\ActivateDealerListingAction;
use App\Action\DealerListing\AddDealerListingAction;
use App\Action\DealerListing\ApproveDealerListingAction;
use App\Action\DealerListing\DeleteDealerListingAction;
use App\Action\DealerListing\GetDealerListingAction;
use App\Models\DealerListing;
use Illuminate\Http\Request;

class DealerListingsController extends Controller
{
    public function add(Request $request)
    {
        $action = new AddDealerListingAction();
        $data = $action->execute($request);
        return $data;
    }

    public function get(Request $request)
    {
        $action = new GetDealerListingAction();
        $data = $action->execute($request);
        return $data;
    }

    public function delete($id)
    {
        $action = new DeleteDealerListingAction();
        $data = $action->execute($id);
        return $data;
    }

    public function approve($id, Request $request)
    {
        $action = new ApproveDealerListingAction();
        $data = $action->execute($id, $request);
        return $data;
    }
    
    public function activate($id, Request $request)
    {
        $action = new ActivateDealerListingAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}
