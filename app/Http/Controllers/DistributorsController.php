<?php

namespace App\Http\Controllers;

use App\Action\Distributor\AddNewDistributorAction;
use App\Action\Distributor\DeleteDistributorAction;
use App\Action\Distributor\EditDistributorAction;
use App\Action\Distributor\GetDistributorAction;
use App\Models\Distributor;
use Illuminate\Http\Request;

class DistributorsController extends Controller
{
    public function addDistributor(Request $request)
    {
        $action = new AddNewDistributorAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getDistributor(Request $request)
    {
        $action = new GetDistributorAction();
        $data = $action->execute($request);
        return $data;
    }

    public function editDistributor(Request $request, $id)
    {
        $action = new EditDistributorAction();
        $data = $action->execute($id, $request);
        return $data;
    }    

    public function deleteDistributor($id)
    {
        $action = new DeleteDistributorAction();
        $data = $action->execute($id);
        return $data;
    }
}
