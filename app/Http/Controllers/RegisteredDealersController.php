<?php

namespace App\Http\Controllers;

use App\Action\RegisteredDealer\AddRegisteredDealerAction;
use App\Action\RegisteredDealer\ApprovedByDistributorAction;
use App\Action\RegisteredDealer\GetRegisteredDealerAction;
use App\Action\RegisteredDealer\RemoveRegisteredDealerAction;
use App\Action\RegisteredDealer\RequestRegistrationByDealerAction;
use App\Http\Requests\StoreRegisteredDealerRequest;
use Illuminate\Support\Facades\Auth;

use App\Models\RegisteredDealer;
use Illuminate\Http\Request;

class RegisteredDealersController extends Controller
{
    public function addDealer(Request $request)
    {
        $action = new RequestRegistrationByDealerAction();
        $data = $action->execute($request);
        return $data; 
    }

    public function getRegisteredDealer(Request $request){
        $action = new GetRegisteredDealerAction();
        $data = $action->execute($request);
        return $data;
    }

    public function removeDealer($id)
    {
        $action = new RemoveRegisteredDealerAction();
        $data = $action->execute($id);
        return $data;
    }

    public function approveDealerRequest($id, Request $request)
    {
        $action = new ApprovedByDistributorAction();
        $data = $action->execute($id, $request);
        return $data;
    }
}
