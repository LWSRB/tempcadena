<?php

namespace App\Http\Controllers;

use App\Action\Address\DeleteAddressAction;
use App\Action\Address\NewAddressAction;
use App\Action\Address\ShowUserAddressesAction;
use App\Action\Address\UpdateAddressAction;
use Illuminate\Http\Request;

class AddressesController extends Controller
{
    public function show($id)
    {
        $action = new ShowUserAddressesAction();
        $record = $action->execute($id);
        return $record;
    }
    
    public function create(Request $request)
    {
        $action = new NewAddressAction();
        $record = $action->execute($request);
        return $record;
    }

    public function update($id, Request $request)
    {
        $action = new UpdateAddressAction();
        $record = $action->execute($id, $request);
        return $record;
    }

    public function destroy($id)
    {
        $action = new DeleteAddressAction();
        $record = $action->execute($id);
        return $record;
    }
}