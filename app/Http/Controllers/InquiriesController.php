<?php

namespace App\Http\Controllers;

use App\Action\Inquiry\AddRemarksAction;
use App\Action\Inquiry\CreateInquiryAction;
use App\Action\Inquiry\DeleteInquiryAction;
use App\Action\Inquiry\GetAllInquiriesAction;
use App\Action\Inquiry\GetInquiryByIdAction;
use App\Action\Inquiry\GetUserInquiryAction;
use App\Http\Requests\StoreInquiryRequest;
use App\Models\Inquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InquiriesController extends Controller
{
    public function newInquiry(StoreInquiryRequest $request)
    {
        $action = new CreateInquiryAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getAllInquiries()
    {
        $action = new GetAllInquiriesAction();
        $data = $action->execute();
        return $data;
    }

    public function getUserInquiries($id)
    {
        $action = new GetUserInquiryAction();
        $data = $action->execute($id);
        return $data;
    }

    public function deleteInquiry($id)
    {
        $action = new DeleteInquiryAction();
        $data = $action->execute($id);
        return $data;
    }

    public function addRemarks($id, Request $request)
    {
        $action = new AddRemarksAction();
        $data = $action->execute($id, $request);
        return $data;
    }

    public function getInquiryById($id)
    {
        $action = new GetInquiryByIdAction();
        $data = $action->execute($id);
        return $data;
    }
}