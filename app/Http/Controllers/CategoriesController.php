<?php

namespace App\Http\Controllers;

use App\Action\Category\CreateCategoryAction;
use App\Action\Category\DeleteCategoryAction;
use App\Action\Category\GetAllCategoriesAction;
use App\Action\Category\UpdateCategoryAction;
use App\Action\Category\SearchCategoryAction;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{

    public function newCategory(Request $request)
    {
        $action = new CreateCategoryAction();
        $data = $action->execute($request);
        return $data;
    }

    public function getCategory(Request $request)
    {
        $action = new GetAllCategoriesAction();
        $data = $action->execute($request);
        return $data;
    }


    public function searchCategory($id)
    {
        $action = new SearchCategoryAction();
        $record = $action->execute($id);
        return $record;
    }


    public function editCategory(Request $request, $id)
    {
        $action = new UpdateCategoryAction();
        $data = $action->execute($request, $id);
        return $data;
    }


    public function deleteCategory($id)
    {
        $action = new DeleteCategoryAction();
        $data = $action->execute($id);
        return $data;
    }
}
