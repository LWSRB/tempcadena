<?php

namespace App\Http\Controllers;

use App\Action\ListingImages\CreateListingImageAction;
use App\Action\ListingImages\DeleteListingImageAction;
use App\Action\ListingImages\GetAllListingImageAction;
use App\Action\ListingImages\GetListingImageByIdAction;
use App\Http\Requests\StoreListingsImageRequest;
use App\Models\ListingsImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListingsImageController extends Controller
{

    public function newListingImage(Request $request)
    {
        $action = new CreateListingImageAction();
        $data = $action->execute($request);
        return $data;
    }


    public function getAllImages()
    {
        $action = new GetAllListingImageAction();
        $data = $action->execute();
        return $data;
    }


    public function searchListingImage($id)
    {
        $action = new GetListingImageByIdAction();
        $data = $action->execute($id);
        return $data;
    }


    public function deleteListingImage($id)
    {
        $action = new DeleteListingImageAction();
        $data = $action->execute($id);
        return $data;
    }
}
