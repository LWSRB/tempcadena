<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'parent_id' => 'required',
            'image' => 'required'
        ];
    }

    public function messages()
    {
        return [
          'name.required' => ['title'=>'Invalid name','message'=>'Name is required.'],
          'description.required' => ['title'=>'Invalid description','message'=>'Description field is required.'],
          'parent_id.required' => ['title'=>'Invalid parent_id','message'=>'Parent_id field is required.'],
          'image.required' => ['title'=>'Invalid image','message'=>'Image field is required.'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      $message = [];
      $error = $validator->errors();
        foreach((array) $error as $validator){
          if(is_array($validator)){
            $list = array_keys($validator);
            foreach($list as $l)
            array_push($message,$validator[$l][0]);
          }
        }

        throw new HttpResponseException(response()->json($message, 422));
    }
}
