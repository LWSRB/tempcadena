<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreCarouselImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'order_no' => 'required',
        ];
    }

    public function messages()
    {
        return [
          'image.required' => ['title'=>'Invalid image','message'=>'image is required.'],
          'order_no.required' => ['title'=>'Invalid order_no','message'=>'order_no field is required.'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      $message = [];
      $error = $validator->errors();
        foreach((array) $error as $validator){
          if(is_array($validator)){
            $list = array_keys($validator);
            foreach($list as $l)
            array_push($message,$validator[$l][0]);
          }
        }

        throw new HttpResponseException(response()->json($message, 422));
    }
}
