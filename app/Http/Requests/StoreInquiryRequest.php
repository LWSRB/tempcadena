<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreInquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'listing_id' => 'required',
            'full_name' => 'required',
            'email' => 'required',
            'contact_number' => 'required',
            'address' => 'required',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
          'listing_id.required' => ['title'=>'Invalid listing_id','message'=>'listing_id field is required.'],
          'full_name.required' => ['title'=>'Invalid full_name','message'=>'full_name field is required.'],
          'email.required' => ['title'=>'Invalid email','message'=>'email field is required.'],
          'contact_number.required' => ['title'=>'Invalid contact_number','message'=>'contact_number field is required.'],
          'address.required' => ['title'=>'Invalid address','message'=>'address field is required.'],
          'message.required' => ['title'=>'Invalid message','message'=>'message field is required.'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      $message = [];
      $error = $validator->errors();
        foreach((array) $error as $validator){
          if(is_array($validator)){
            $list = array_keys($validator);
            foreach($list as $l)
            array_push($message,$validator[$l][0]);
          }
        }

        throw new HttpResponseException(response()->json($message, 422));
    }
}
