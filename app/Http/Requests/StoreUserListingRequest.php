<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUserListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'listing_id' => 'required',
            'description' => 'required',
            'status' => 'required',
            'featured_expiration' => 'required',
            'discount' => 'required',
            'price' => 'required',
            'promo_type' => 'required',
            'promo_expiration' => 'required',
        ];
    }

    public function messages()
    {
        return [
          'listing_id.required' => ['title'=>'Invalid listing_id','message'=>'listing_id is required.'],
          'description.required' => ['title'=>'Invalid description','message'=>'description field is required.'],
          'status.required' => ['title'=>'Invalid status','message'=>'status field is required.'],
          'featured_expiration.required' => ['title'=>'Invalid featured_expiration','message'=>'featured_expiration field is required.'],
          'discount.required' => ['title'=>'Invalid discount','message'=>'discount field is required.'],
          'price.required' => ['title'=>'Invalid price','message'=>'price field is required.'],
          'promo_type.required' => ['title'=>'Invalid promo_type','message'=>'promo_type field is required.'],
          'promo_expiration.required' => ['title'=>'Invalid promo_expiration','message'=>'promo_expiration field is required.'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      $message = [];
      $error = $validator->errors();
        foreach((array) $error as $validator){
          if(is_array($validator)){
            $list = array_keys($validator);
            foreach($list as $l)
            array_push($message,$validator[$l][0]);
          }
        }

        throw new HttpResponseException(response()->json($message, 422));
    }
}
