<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreListingRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'category_id' => 'required',
      'sub_category_id' => 'required',
      'brand_id' => 'required',
      'series' => 'required',
      'product_model' => 'required',
      'color' => 'required',
      'description' => 'required',
      'price_from' => 'required',
      'price_to' => 'required',
      'warranty' => 'required',
    ];
  }

  public function messages()
  {
    return [
      'category_id.required' => ['title' => 'Invalid category', 'message' => 'Category is required.'],
      'sub_category_id.required' => ['title' => 'Invalid sub-category', 'message' => 'Sub-category field is required.'],
      'brand_id.required' => ['title' => 'Invalid brand', 'message' => 'Brand field is required.'],
      'series.required' => ['title' => 'Invalid series', 'message' => 'Series field is required.'],
      'product_model.required' => ['title' => 'Invalid product model', 'message' => 'Product model field is required.'],
      'color.required' => ['title' => 'Invalid color', 'message' => 'Color field is required.'],
      'description.required' => ['title' => 'Invalid description', 'message' => 'Description field is required.'],
      'price_from.required' => ['title' => 'Invalid price range', 'message' => 'Price range field is required.'],
      'price_to.required' => ['title' => 'Invalid price range', 'message' => 'Price range field is required.'],
      'warranty.required' => ['title' => 'Invalid warranty', 'message' => 'Warrantty field is required.'],
    ];
  }

  protected function failedValidation(Validator $validator)
  {
    $message = [];
    $error = $validator->errors();
    foreach ((array) $error as $validator) {
      if (is_array($validator)) {
        $list = array_keys($validator);
        foreach ($list as $l)
          array_push($message, $validator[$l][0]);
      }
    }

    throw new HttpResponseException(response()->json($message, 422));
  }
}
