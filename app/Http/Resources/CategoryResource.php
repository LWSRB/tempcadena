<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'type' => 'Category',
            'attributes' => [
                'name' => (string)$this->name,
                'description' => (string)$this->description,
                'parent_id' => (string)$this->parent_id,
                'image' => (string)$this->image,
            ]
        ];
    }
}
