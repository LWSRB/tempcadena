<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompareListing extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'listing_id_1',
        'listing_id_2',
        'listing_id_3',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $table = 'compare_listings';

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function listing()
    {
        return $this->hasOne(Listing::class);
    }
}
