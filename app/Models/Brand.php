<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'image_path',
        'is_approved'
    ];

    protected $table = 'brands';

    protected $primaryKey = 'id';

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

    public function distributors()
    {
        return $this->hasMany(User::class);
    }
}
