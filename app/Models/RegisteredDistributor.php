<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisteredDistributor extends Model
{
    use HasFactory;

    protected $fillable = [
        'distributor_id',
        'dealer_id',
        'register_date',
        'is_approved '
    ];

    protected $table = 'registered_distributors';

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function distributor()
    {
        return $this->hasOne(Distributor::class, 'id');
    }
}
