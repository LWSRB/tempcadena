<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'user_id',
        'listing_id',
        'dealer_id',
    ];

    protected $table = 'wishlists';

    protected $primaryKey = 'id';

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function listing()
    {
        return $this->hasOne(Listing::class);
    }
}
