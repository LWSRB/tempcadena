<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisteredDealer extends Model
{
    use HasFactory;

    protected $fillable = [
        'dealer_id',
        'distributor_id',
        'register_date',
        'is_approved '
    ];

    protected $table = 'registered_dealers';

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo(Distributor::class);
    }

    public function dealer()
    {
        return $this->hasOne(User::class, 'id');
    }
}
