<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'listing_id',
        'full_name',
        'email',
        'contact_number',
        'address',
        'message',
        'remarks',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $table = 'inquiries';

    protected $primaryKey = 'id';

    public function listings()
    {
        return $this->belongsTo(Listing::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
