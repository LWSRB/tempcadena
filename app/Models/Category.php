<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'sub_category_of',
        'image_path'
    ];

    protected $hidden = [];

    protected $table = 'categories';

    protected $primaryKey = 'id';

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }
}
