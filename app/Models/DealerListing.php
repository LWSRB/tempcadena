<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DealerListing extends Model
{
    use HasFactory;

    protected $fillable = [
        'dealer_id',
        'listing_id',
        'is_approved',
        'is_active',
    ];

    protected $primaryKey = 'id';

    public function dealer()
    {
        return $this->belongsTo(User::class);
    }

    public function listing()
    {
        return $this->hasOne(Listing::class, 'id');
    }
}