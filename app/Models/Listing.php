<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;

    protected $table = 'listings';

    protected $primaryKey = 'id';

    protected $fillable = [
        'distributor_id',
        'category_id',
        'sub_category_id',
        'brand_id',
        'series',
        'product_model',
        'color',
        'description',
        'price_from',
        'price_to',
        'warranty',
        'no_of_views',
        'is_approved',
        'is_active',
        'is_public',
        'updated_at',
        'created_at',
    ];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function images()
    {
        return $this->hasMany(ListingsImage::class);
    }

    public function image()
    {
        return $this->hasOne(ListingsImage::class);
    }

    public function promos()
    {
        return $this->hasMany(UserListing::class);
    }
}
