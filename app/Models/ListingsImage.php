<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListingsImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'image_path',
        'listing_id',
    ];

    protected $table = 'listings_images';

    protected $primaryKey = 'id';

    public function listings()
    {
        return $this->belongsTo(Listing::class);
    }
}
