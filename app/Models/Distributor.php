<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Distributor extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'description',
        'contact_number',
        'email',
        'address_1',
        'address_2',
        'city',
        'province',
        'region',
        'postal_code',
        'lat',
        'long',
    ];

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

    public function dealers()
    {
        return $this->hasMany(RegisteredDealer::class);
    }

    public function registrations()
    {
        return $this->belongsTo(RegisteredDistributor::class);
    }
}
