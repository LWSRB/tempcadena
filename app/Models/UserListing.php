<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserListing extends Model
{
    use HasFactory;

    protected $table = 'user_listings';

    protected $primaryKey = 'id';

    protected $fillable = [
        'listing_id',
        'dealer_id',
        'description',
        'is_active',
        'featured_expiration',
        'discount',
        'price',
        'promo_type',
        'promo_expiration',
        'created_at',
        'updated_at',
    ];

    protected $cast = [
        'featured_expiration' => 'datetime:Y/m/d',
        'promo_expiration' => 'datetime:Y/m/d',
    ];

    public function listings()
    {
        return $this->belongsTo(Listing::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dealer()
    {
        return $this->hasOne(User::class);
    }

    public function listing()
    {
        return $this->hasOne(Listing::class);
    }
}
