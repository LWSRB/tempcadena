<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'role',
        'department_id',
        'name',
        'email',
        'password',
        'mobile_number',
        'landline_number',
        'current_user_lat',
        'current_user_long',

        'avatar',
        'provider',
        'provider_id',
        'access_token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'role',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'users';

    protected $primaryKey = 'id';

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

    public function linkedSocialAccounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function registeredDealers()
    {
        return $this->hasMany(RegisteredDealer::class);
    }

    public function registeredDistributors()
    {
        return $this->hasMany(RegisteredDistributor::class);
    }

    public function compareListing()
    {
        return $this->hasOne(CompareListing::class);
    }
}