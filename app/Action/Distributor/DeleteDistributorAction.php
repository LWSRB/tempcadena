<?php

namespace App\Action\Distributor;

use App\Models\Distributor;
use Illuminate\Support\Facades\Auth;

class DeleteDistributorAction
{
    public function execute($id)
    {
        if(Auth::check() && Auth::user()->role === 'admin'){
            if (Distributor::whereId($id)->count() == 0) {
                return response()->json('Distributor Entry not found', 404);
            }

            $data = Distributor::whereId($id)->delete();
            if($data){
                return response()->json('Distributor Entry Deleted', 200);
            } else {
                return response()->json('Error', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}