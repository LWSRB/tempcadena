<?php

namespace App\Action\Distributor;

use App\Models\Distributor;
use Illuminate\Support\Facades\Auth;

class EditDistributorAction
{
    public function execute($id, $request)
    {
        if(Auth::check() && Auth::user()->role === 'admin'){

            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'contact_number' => 'required',
                'email' => 'required',
                'address_1' => 'required',
                'address_2' => 'required',
                'city' => 'required',
                'province' => 'required',
                'region' => 'required',
                'postal_code' => 'required',
            ]);
            $input = $request->all();

            // $concatAddress = $input['address_1'].'+'.$input['address_2'].'+'.$input['city'].'+'.$input['province'].'+'.$input['region'];
            // $preparedAddress = str_replace(' ', '+', $concatAddress);
            // $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?new_forward_geocoder=true&address='.$preparedAddress.'&key='.$api_key);
            // $output = json_decode($geocode);
            // // dd($output);

            // $latitude = $output->results[0]->geometry->location->lat;
            // $longitude = $output->results[0]->geometry->location->lng;

            $latitude = 23.697809;
            $longitude = 120.960518;

            $data = Distributor::whereId($id)->update([
                'name' => $input['name'],
                'description' => $input['description'],
                'contact_number' => $input['contact_number'],
                'email' => $input['email'],
                'address_1' => $input['address_1'],
                'address_2' => $input['address_2'],
                'city' => $input['city'],
                'province' => $input['province'],
                'region' => $input['region'],
                'postal_code' => $input['postal_code'],
                'lat' => $latitude,
                'long' => $longitude
            ]);

            if($data)
                return response()->json('Success', 200);
                else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
