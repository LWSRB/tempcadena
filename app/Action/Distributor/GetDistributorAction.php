<?php

namespace App\Action\Distributor;

use App\Models\Distributor;

class GetDistributorAction
{
    public function execute($request)
    {
        if (Distributor::count() == 0) {
            return response()->json(null, 404);
        }

        if($request->has('limit'))
        {
            $data = Distributor::paginate($request->limit);
            return response()->json($data, 200);
        }

        if($request->has('id'))
        {
            $data = Distributor::whereId($request->id)->get();
            return response()->json($data, 200);
        }

        $data = Distributor::all();
        return response()->json($data, 200);
    }
}