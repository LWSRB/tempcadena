<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Stevebauman\Location\Facades\Location;

class UserLoginAction
{

  public $success_status = 200;

  public function execute($request)
  {
    $request->validate([
      'email' => 'email',
    ]);

    if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
      // $ip = \Request::ip();
      $ip = '141.47.82.112';
      $data = Location::get($ip);

      $user = Auth::user();

      $user->update([
        'current_user_lat' => $data->latitude,
        'current_user_long' => $data->longitude
      ]);

      $success['token'] =  $user->createToken('authToken')->accessToken;

      if ($success)
        return response()->json($success, $this->success_status);
      else
        return response()->json('Error', 400);
    } else {
      return response()->json('Unauthorized', 401);
    }
  }
}