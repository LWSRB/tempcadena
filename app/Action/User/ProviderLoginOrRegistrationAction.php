<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;

class ProviderLoginOrRegistrationAction
{
  public function execute($request)
  {
    $driver = $request;
    if (!$this->isProviderAllowed($driver)) {
      return response()->json('error', 400);
    }
    Socialite::driver($driver)->redirect();
    $user = Socialite::driver($driver)->user();
    return empty($user->email) ?
      response()->json('error', 400)
      :
      $this->loginOrCreateAccount($user, $driver);
  }

  protected function loginOrCreateAccount($providerUser, $driver)
  {
    $user = User::where('email', $providerUser->getEmail())->first();

    if ($user) {
      $user->update([
        'avatar' => $providerUser->avatar,
        'provider' => $driver,
        'provider_id' => $providerUser->id,
        'access_token' => $providerUser->token
      ]);
    } else {
      $user = User::create([
        'name' => $providerUser->getName(),
        'email' => $providerUser->getEmail(),
        'avatar' => $providerUser->getAvatar(),
        'provider' => $driver,
        'provider_id' => $providerUser->getId(),
        'access_token' => $providerUser->token,
        'password' => ''
      ]);
    }

    $success['token'] = $user->createToken('authToken')->accessToken;
    Auth::login($user, true);

    return response()->json($success, 200);
  }

  protected $providers = [
    'facebook', 'google',
  ];

  private function isProviderAllowed($driver)
  {
    return in_array($driver, $this->providers) && config()->has("services.{$driver}");
  }
}
