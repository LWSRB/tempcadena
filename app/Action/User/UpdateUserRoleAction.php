<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Carbon\Carbon;

class UpdateUserRoleAction
{

  public $success_status = 200;
  public function execute($id, $request)
  {
    $input = $request->all();
    if (Auth::check() && Auth::user()->role == 'admin') {
      $data = User::whereId($id)->update([
        'role' => $input['role'],
      ]);
      if ($data)
        return response()->json($data, $this->success_status);
      else
        return response()->json('Error', 400);
    }
  }
}
