<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UpdateUserDetailsAction
{
    public function execute($id, $request)
    {
        $input = $request->all();
        if (Auth::check() && Auth::user()->id == $id) {
            $data = User::whereId($id)->update([
                'name' => $input['name'],
                'mobile_number' => $input['mobile_number'],
                'landline_number' => $input['landline_number'],
            ]);
            $updated = User::with('addresses')->whereId($id)->get();

            if ($data)
                return response()->json($updated, 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}