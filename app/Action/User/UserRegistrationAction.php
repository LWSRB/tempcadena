<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;


class UserRegistrationAction
{
  public $success_status = 200;
  public function execute($request)
  {
    $request->validate([
      'email' => 'email',
    ]);

    $input = $request->all();
    $input['password'] = bcrypt($input['password']);

    $user = User::create($input);
    $success['token'] =  $user->createToken('authToken')->accessToken;
    $success['email'] =  $user->email;

    if ($success)
      return response()->json($success, $this->success_status);
    else
      return response()->json('Error', 400);
  }
}