<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class GetDealersListAction
{
    public function execute($request)
    {
        if(User::where(['role' => 'dealer'])->count() === 0){
            return response()->json('No dealers found', 200);
        }

        if($request->has('limit'))
        {
            $data = User::with('addresses')->where(['role' => 'dealer'])->paginate($request->limit);
            if ($data)
                return response()->json($data, 200);
            else
                return response()->json('Error', 400);
        }

        $data = User::with('addresses')->where(['role' => 'dealer'])->get();
        if ($data)
            return response()->json($data, 200);
        else
            return response()->json('Error', 400);
    }
}