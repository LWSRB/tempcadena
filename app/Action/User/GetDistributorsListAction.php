<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class GetDistributorsListAction
{
    public function execute($request)
    {
        if(User::where(['role' => 'distributor'])->count() === 0){
            return response()->json('No distributor found', 200);
        }

        if($request->has('limit'))
        {
            $data = User::with('addresses')->where(['role' => 'distributor'])->paginate($request->limit);
            if ($data)
                return response()->json($data, 200);
            else
                return response()->json('Error', 400);
        }


        $data = User::with('addresses')->where(['role' => 'distributor'])->get();
        if ($data)
            return response()->json($data, 200);
        else
            return response()->json('Error', 400);
    }
}
