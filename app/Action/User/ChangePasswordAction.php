<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ChangePasswordAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::id() == $id) {

            $input = $request->all();

            if (!(Hash::check($input['current_password'], Auth::user()->password))) {
                return response()->json('Incorrect Password', 400);
            } else {
                if ($input['new_password'] == $input['confirm_new_password']) {

                    $user = User::whereId($id)->update([
                        'password' => bcrypt($input['new_password'])
                    ]);
                    if ($user)
                        return response()->json(['success' => 'password change successful'], 200);
                    else
                        return response()->json(['error' => 'bad request'], 400);
                } else {
                    return response()->json(['error' => 'new password & confirm password does not match'], 400);
                }
            }

        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
