<?php

namespace App\Action\User;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Stevebauman\Location\Facades\Location;

class FacebookLoginAction
{
	public function execute()
	{
        $user = Socialite::driver('facebook')->user();

        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name' => $user->name,
            'password' => Hash::make(Str::random(24))
        ])

        Auth::login($user, true);

	    // $ip = \Request::ip();
	    $ip = '141.47.82.112';
	    $data = Location::get($ip);

        $userDetails = Auth::user()
        $userDetails->update([
	        'current_user_lat' => $data->latitude,
	        'current_user_long' => $data->longitude
        ])

        $success['token'] =  $user->createToken('authToken')->accessToken;

	    if ($success)
	        return response()->json($success, 200);
	    else
	        return response()->json('Error', 400);
	    } else {
	      return response()->json('Unauthorized', 401);
	    }
	}
}