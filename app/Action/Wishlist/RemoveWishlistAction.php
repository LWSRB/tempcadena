<?php

namespace App\Action\Wishlist;

use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;

class RemoveWishlistAction
{
    public function execute($data)
    {
        if (Auth::check() && Auth::user()->role == 'user') {
            if (Wishlist::find($data) != null) {
                Wishlist::whereId($data)->delete();
                return response()->json(null, 200);
            } else {
                return response()->json(null, 404);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
