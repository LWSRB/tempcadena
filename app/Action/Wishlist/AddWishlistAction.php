<?php

namespace App\Action\Wishlist;

use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;

class AddWishlistAction
{
    public $success_status = 200;
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'user') {
            $input = $request->all();
            $data = Wishlist::create([
                'type' => $input['type'],
                'user_id' => Auth::id(),
                'listing_id' => $input['listing_id'],
                'dealer_id' => $input['dealer_id'],
            ]);
            return response()->json($data, $this->success_status);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
