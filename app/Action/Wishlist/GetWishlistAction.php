<?php

namespace App\Action\Wishlist;

use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;

class GetWishlistAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'user') {
            if (Wishlist::count() == 0) return response()->json(null, 204);

            $user = Auth::id();

            if ($request->has('limit'))
            {
                if ($request->has('id'))
                {
                    $data = Wishlist::where(['id' => $request->id, 'user_id' => $user])->paginate($request->limit);
                    return response()->json($data, 200);
                }
    
                $data = Wishlist::where(['user_id' => $user])->paginate($request->limit);
    
                if ($data)
                    return response()->json($data, 200);
                else
                    return response()->json(null, 204);
            }

            if ($request->has('id'))
            {
                $data = Wishlist::where(['id' => $request->id, 'user_id' => $user])->get();
                return response()->json($data, 200);
            }

            $data = Wishlist::where(['user_id' => $user])->get();

            if ($data)
                return response()->json($data, 200);
            else
                return response()->json(null, 204);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
