<?php

namespace App\Action\Listing;

use App\Models\Listing;

class SearchListingAction
{
    public $success_status = 200;
    public function execute($id)
    {
        $record = Listing::with('images')->find($id);

        if ($record == null) {
            return response()->json(null, 404);
        } else {
            Listing::find($id)->increment('no_of_views');
            return response()->json($record, $this->success_status);
        }
    }
}
