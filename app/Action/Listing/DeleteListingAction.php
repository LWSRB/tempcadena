<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class DeleteListingAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' || Auth::user()->role == 'admin') {
            if(Listing::whereId($id)->count() === 0) return response()->json('No listing with id#'.$id.' found', 404);

            if (Auth::user()->role == 'admin') {
                Listing::whereId($id)->delete();
                return response()->json('Successfully deleted', 200);
            }

            return response()->json(['error' => 'not found'], 404);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
