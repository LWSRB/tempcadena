<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class PublicListingAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $data = Listing::whereId($id);
            
            if($data->count() === 0) return response()->json('No listing with id#'.$id.' found.', 404);
            
            $update = $data->update([
                'is_public' => $request->status
            ]);

            if($update){
                if($request->status == 1) return response()->json('Listing now public', 200);
                return response()->json('Listing now private', 200);
            }else{
                return response()->json('Error', 400);
            }
        }
        return response()->json('Unauthorized', 401);
    }
}