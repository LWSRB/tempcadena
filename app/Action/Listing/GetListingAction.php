<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class GetListingAction
{
    public function execute($request)
    {
        if (Listing::count() == 0) return response()->json('No listing found', 404);

        if($request->has('limit'))
        {
            if($request->has('is_approved') || $request->has('is_active') || $request->has('is_public')){
                $listing = Listing::with('image')->where([
                    'is_approved' => $request->is_approved,
                    'is_active' => $request->is_active,
                    'is_public' => $request->is_public
                ])->paginate($request->limit);
                return response()->json($listing, 200);
            }

            $listing = Listing::with('image')->paginate($request->limit);
            return response()->json($listing, 200);
        }

        if($request->has('is_approved') || $request->has('is_active') || $request->has('is_public')){
            $listing = Listing::with('image')->where([
                'is_approved' => $request->is_approved,
                'is_active' => $request->is_active,
                'is_public' => $request->is_public
            ])->get();
            return response()->json($listing, 200);
        }

        $listing = Listing::with('image')->get();

        if($listing)
            return response()->json($listing, 200);
        else
            return response()->json('Error', 400);
    }
}