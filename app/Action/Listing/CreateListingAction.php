<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class CreateListingAction
{
    public $success_status = 200;
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            // $userId = Auth::user()->id;
            $input = $request->all();
            $category = Listing::create([
                'distributor_id' => $input['distributor_id'],
                'category_id' => $input['category_id'],
                'sub_category_id' => $input['sub_category_id'],
                'brand_id' => $input['brand_id'],
                'series' => $input['series'],
                'product_model' => $input['product_model'],
                'color' => $input['color'],
                'description' => $input['description'],
                'price_from' => $input['price_from'],
                'price_to' => $input['price_to'],
                'warranty' => $input['warranty'],
            ]);
            if ($category) {
                return response()->json('Listing Created', 200);
            } else {
                return response()->json('Error', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}