<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class ActivateListingAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $data = Listing::whereId($id);
            
            if($data->count() === 0) return response()->json('No listing with id#'.$id.' found.', 404);
            
            $update = $data->update([
                'is_active' => $request->status
            ]);

            if($update){
                if($request->status == 1) return response()->json('Listing now active', 200);
                return response()->json('Listing deactivated', 200);
            }else{
                return response()->json('Error', 400);
            }
        }
        return response()->json('Unauthorized', 401);
    }
}
