<?php

namespace App\Action\Listing;

use App\Models\Listing;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UpdateListingAction
{
    public function execute($request, $id)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' || 'admin') {
            $input = $request->all();
            $category = Listing::whereId($id)->update([
                'category_id' => $input['category_id'],
                'sub_category_id' => $input['sub_category_id'],
                'brand_id' => $input['brand_id'],
                'series' => $input['series'],
                'product_model' => $input['product_model'],
                'color' => $input['color'],
                'description' => $input['description'],
                'price_from' => $input['price_from'],
                'price_to' => $input['price_to'],
                'warranty' => $input['warranty'],
                'updated_at' => Carbon::now()
            ]);
            if ($category)
                return response()->json('Updated', 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
