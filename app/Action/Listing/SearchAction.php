<?php

namespace App\Action\Listing;

use App\Models\Brand;
use App\Models\Listing;

class SearchAction
{
    public function execute($request)
    {
        $input = $request->all();
        $listing = Listing::query();
        $brand = Brand::query();

        if ($request->has('query')) {

            $tempdata = $listing->with('image')
                ->where('brand_id', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('product_model', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('price_from', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('price_to', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('description', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('series', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('color', 'LIKE', '%' . $input['query'] . '%')
                ->orWhere('warranty', 'LIKE', '%' . $input['query'] . '%');

            $data = $tempdata->where([
                'is_approved' => 1,
                'is_active' => 1,
                'is_public' => 1
            ]);

            if ($request->has('limit'))
                return response()->json($data->paginate($request->limit), 200);
            else
                return response()->json($data->get(), 200);
        }

        if($request->has('search_brand') || $request->has('search_series') || $request->has('search_product_model')){

            $brandPlaceHolder = $brand->where([
                ['name', 'LIKE', '%' . $input['search_brand'] . '%']
            ])->get();
            
            $tempdata = $listing->with('image')
            ->where([
                ['brand_id', 'LIKE', '%' . $brandPlaceHolder[0]->id . '%'],
                ['series', 'LIKE', '%' . $input['search_series'] . '%'],
                ['product_model', 'LIKE', '%' . $input['search_product_model'] . '%']
            ]);

            $data = $tempdata->where([
                'is_approved' => 1,
                'is_active' => 1,
                'is_public' => 1
            ]);

            if ($request->has('limit'))
                return response()->json($data->paginate($request->limit), 200);
            else
                return response()->json($data->get(), 200);
        }
        return response()->json(['error' => 'not found'], 404);
    }
}
