<?php

namespace App\Action\UserListing;

use App\Models\UserListing;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CreateUserListingAction
{
    public function execute($request)
    {
        $request->validate([
            'listing_id' => 'required',
            'description' => 'required',
            'promo_expiration' => 'required',
            'promo_type' => 'required',
        ]);

        $input = $request->all();

        if (Auth::check() && Auth::user()->role != 'user') {
            $data = UserListing::create([
                'listing_id' => $input['listing_id'],
                'dealer_id' => $input['dealer_id'],
                'description' => $input['description'],
                'featured_expiration' => $input['featured_expiration'], //format yyyy/mm/dd
                'discount' => $input['discount'],
                'price' => $input['price'],
                'promo_type' => $input['promo_type'],
                'promo_expiration' => $input['promo_expiration'],//format yyyy/mm/dd
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            if ($data)
                return response()->json($data, 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}