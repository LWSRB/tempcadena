<?php

namespace App\Action\UserListing;

use App\Models\UserListing;

class SearchUserListingAction
{
    public function execute($id, $request)
    {
        if ($request->has('is_active'))
        {
            $record = UserListing::where(['id' => $id, 'is_active' => $request->is_active])->get();
            if($record)
                return response()->json($record, 200);
            else
                return response()->json('Not found', 204);
        }

        $record = UserListing::whereId($id)->get();
        if ($record == null) {
            return response()->json('Not found', 204);
        } else {
            return response()->json($record, 200);
        }
    }
}