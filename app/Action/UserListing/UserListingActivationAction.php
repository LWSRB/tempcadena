<?php

namespace App\Action\UserListing;

use App\Models\UserListing;
use Illuminate\Support\Facades\Auth;

class UserListingActivationAction
{
    public function execute($id, $request)
    {
        if(UserListing::whereId($id)->count() <= 0) return response()->json('No promo with ID#'.$id.' found.', 404);

        if (Auth::check() && Auth::user()->role == 'admin')
        {
            $data = UserListing::whereId($id)->update([
                'is_active' => $request->is_active
            ]);

            if($data)
                return response()->json('Activated', 200);
            else
                return response()->json('Error', 400);
        }
        
        if (Auth::check() && Auth::user()->role == 'dealer'){
            $data = UserListing::where(['id' => $id, 'dealer_id' => Auth::id()])->update([
                'is_active' => $request->is_active
            ]);

            if($data)
            return response()->json('Activated', 200);
            else
                return response()->json('Error', 400);
        }
        return response()->json('Unauthorized', 401);
    }
}