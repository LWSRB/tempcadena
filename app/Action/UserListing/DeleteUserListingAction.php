<?php

namespace App\Action\UserListing;

use App\Models\UserListing;
use Illuminate\Support\Facades\Auth;

class DeleteUserListingAction
{
    public function execute($id)
    {
        if(Auth::check() && Auth::user()->role == 'dealer' || Auth::user()->role == 'admin'){
            if (UserListing::whereId($id)->count() <= 0) return response()->json('Promo with ID#'.$id.' not found.', 404);

            $data = UserListing::whereId($id)->delete();

            if($data)
                return response()->json(['success' => 'successfully deleted'], 200);
            else
                return response()->json('Error', 404);
            
        }
        return response()->json('Unauthorized', 401);
    }
}