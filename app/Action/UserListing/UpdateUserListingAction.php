<?php

namespace App\Action\UserListing;

use App\Models\UserListing;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UpdateUserListingAction
{
    public $success_status = 200;
    public function execute($request, $id)
    {
        $input = $request->all();

        if(Auth::check() && Auth::user()->role == 'admin')
        {
            $data = UserListing::whereId($id)->update([
                'description' => $input['description'],
                'featured_expiration' => $input['featured_expiration'],
                'discount' => $input['discount'],
                'price' => $input['price'],
                'promo_type' => $input['promo_type'],
                'promo_expiration' => $input['promo_expiration'],
            ]);

            if ($data)
                return response()->json('promo edited', $this->success_status);
            else
                return response()->json('error', 401);
        }

        if (Auth::check() && Auth::user()->role == 'dealer')
        {
            $data = UserListing::where(['id' => $id, 'dealer_id' => Auth::id()])->update([
                'description' => $input['description'],
                'featured_expiration' => $input['featured_expiration'],
                'discount' => $input['discount'],
                'price' => $input['price'],
                'promo_type' => $input['promo_type'],
                'promo_expiration' => $input['promo_expiration'],
            ]);

            if ($data)
                return response()->json('promo edited', $this->success_status);
            else
                return response()->json('error', 401);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }
}