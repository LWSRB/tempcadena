<?php

namespace App\Action\UserListing;

use App\Models\UserListing;

class GetAllPromoAction
{
    public function execute($request)
    {
        if (UserListing::all()->count() > 0){
            if($request->has('limit')){
                if($request->has('is_active'))
                {
                    $data = UserListing::where(['is_active' => $request->is_active])->paginate($request->limit);
                    if($data)
                        return response()->json($data, 200);
                    else
                        return response()->json('error', 400);
                }

                $data = UserListing::paginate($request->limit);
                if($data)
                    return response()->json($data, 200);
                else
                    return response()->json('error', 400);
            }

            if($request->has('is_active'))
            {
                $data = UserListing::where(['is_active' => $request->is_active])->get();
                if($data)
                    return response()->json($data, 200);
                else
                    return response()->json('error', 400);
            }
            $data = UserListing::all();
            return response()->json($data, 200);
        }
    }
}