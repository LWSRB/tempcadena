<?php

namespace App\Action\DealerListing;

use App\Models\DealerListing;
use Illuminate\Support\Facades\Auth;

class AddDealerListingAction
{
    public function execute($request)
    {
        if(Auth::check() && Auth::user()->role == 'dealer')
        {
            $request->validate([
                'listing_id' => 'required'
            ]);

            $entry = DealerListing::where(['dealer_id' => Auth::id(), 'listing_id' => $request->listing_id])->count();
            if($entry >  0) return response()->json('dealer listing exist', 400);

            $data = DealerListing::create([
                'dealer_id' => Auth::id(),
                'listing_id' => $request->listing_id
            ]);

            if($data)
                return response()->json($data, 200);
            else
                return response()->json('Error', 400);
        }
        return response()->json('Unauthorized', 401);
    }
}