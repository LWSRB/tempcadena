<?php

namespace App\Action\DealerListing;

use App\Models\DealerListing;
use Illuminate\Support\Facades\Auth;

class GetDealerListingAction
{
    public function execute($request)
    {
        if(DealerListing::all()->count() == 0) return response()->json(null, 204);

        if($request->has('id'))
        {
            if($request->has('is_approved'))
            {
                $data = DealerListing::with('listing')->where(['id' => $request->id, 'is_approved' => $request->is_approved])->get();
                return response()->json($data, 200);
            }
            if($request->has('is_active'))
            {
                $data = DealerListing::with('listing')->where(['id' => $request->id, 'is_active' => $request->is_active])->get();
                return response()->json($data, 200);
            }
            $data = DealerListing::with('listing')->whereId($request->id)->get();
            return response()->json($data, 200);
        }

        if($request->has('limit'))
        {
            if($request->has('is_approved'))
            {
                $data = DealerListing::with('listing')->where(['is_approved' => $request->is_approved])->paginate($request->limit);
                return response()->json($data, 200);
            }
            if($request->has('is_active'))
            {
                $data = DealerListing::with('listing')->where(['is_active' => $request->is_active])->paginate($request->limit);
                return response()->json($data, 200);
            }
            $data = DealerListing::with('listing')->paginate($request->limit);
            return response()->json($data, 200);
        }

        $data = DealerListing::with('listing')->get();
        if($data)
            return response()->json($data, 200);
        else
            return response()->json('Error', 400);
    }
}
