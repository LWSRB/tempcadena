<?php

namespace App\Action\DealerListing;

use App\Models\DealerListing;
use Illuminate\Support\Facades\Auth;

class ActivateDealerListingAction
{
    public function execute($id, $request)
    {
        if(Auth::check() && Auth::user()->role == 'dealer')
        {
            if(DealerListing::whereId($id)->count() <= 0) return response()->json('Dealer listing with ID#'.$id.' not found', 404);
            
            $data = DealerListing::where(['id' => $id, 'dealer_id' => Auth::id()])->update([
                'is_active' => $request->is_active
            ]);

            if($data){
                if($request->is_active == 1){
                    return response()->json('activated', 200);
                } else {
                    return response()->json('deactivated', 200);
                }
            } else {
                return response()->json('Error', 400);
            }
        }
        return response()->json('Unauthorized', 401);
    }
}