<?php

namespace App\Action\DealerListing;

use App\Models\DealerListing;
use App\Models\Listing;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Notifications\RegistrationConfirmation;
use Illuminate\Support\Facades\Notification;

class ApproveDealerListingAction
{
    public function execute($id, $request)
    {
        if(Auth::check() && Auth::user()->role == 'admin')
        {
            if(DealerListing::whereId($id)->count() <= 0) return response()->json('Dealer listing with ID#'.$id.' not found', 404);
            
            $data = DealerListing::whereId($id)->update([
                'is_approved' => $request->is_approved
            ]);

            if($data){
                $placeholderId = DealerListing::whereId($id)->get();
                $user = User::whereId($placeholderId[0]->dealer_id)->get();
                $listing = Listing::whereId($placeholderId[0]->listing_id)->get();

                if($request->is_approved == 1){
                    $approvedNotification = [
                        'body' => 'Your request to add '.$listing[0]->product_model.' listing has been approved.',
                        'text' => 'Go to Cadena',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($user, new RegistrationConfirmation($approvedNotification));

                    return response()->json('approved', 200);
                } else {
                    $declinedNotification = [
                        'body' => 'Your request to add '.$listing[0]->product_model.' listing has been declined.',
                        'text' => 'Go to Cadena',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($user, new RegistrationConfirmation($declinedNotification));
                    
                    return response()->json('declined', 200);
                }
            } else {
                return response()->json('Error', 400);
            }
        }
        return response()->json('Unauthorized', 401);
    }
}
