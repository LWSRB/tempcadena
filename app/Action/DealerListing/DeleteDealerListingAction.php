<?php

namespace App\Action\DealerListing;

use App\Models\DealerListing;
use Illuminate\Support\Facades\Auth;

class DeleteDealerListingAction
{
    public function execute($id)
    {
        if(Auth::check() && Auth::user()->role == 'dealer')
        {
            if(DealerListing::whereId($id)->count() <= 0) return response()->json('Dealer listing with ID#'.$id.' not found', 404);
            
            $data = DealerListing::where(['id' => $id, 'dealer_id' => Auth::id()])->delete();

            if($data)
                return response()->json('Deleted', 200);
            else
                return response()->json('Error', 400);
        }
        return response()->json('Unauthorized', 401);
    }
}
