<?php

namespace App\Action\Category;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UpdateCategoryAction
{
    public function execute($request, $id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $input = $request->all();
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);

            $image = $request->file('image');
            $imageName = time() . '-category-image' . '.' . $image->guessClientExtension();
            Storage::disk('category')->put($imageName, file_get_contents($image));

            if($request->has('sub_category_of'))
            {
                $record = Category::whereId($id)->update([
                    'name' => $input['name'],
                    'description' => $input['description'],
                    'sub_category_of' => $input['sub_category_of'],
                    'image_path' => Storage::disk('category')->url($imageName),
                ])->get();
    
                if ($record)
                    return response()->json('Category successfully edited', 200);
                else
                    return response()->json('Error', 400);
            }

            $record = Category::whereId($id)->update([
                'name' => $input['name'],
                'description' => $input['description'],
                'image_path' => Storage::disk('category')->url($imageName),
            ]);

            if ($record)
                return response()->json('Category successfully edited', 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
