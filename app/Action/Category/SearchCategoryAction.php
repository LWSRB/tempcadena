<?php

namespace App\Action\Category;

use App\Models\Category;

class SearchCategoryAction
{
    public $success_status = 200;
    public function execute($id)
    {
        $record = Category::find($id);

        if ($record == null)
            return response()->json('Not found', 404);
        else
            return response()->json($record, $this->success_status);
    }
}
