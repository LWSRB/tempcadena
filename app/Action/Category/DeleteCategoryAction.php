<?php

namespace App\Action\Category;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DeleteCategoryAction
{
    public $success_status = 200;
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            if (Category::find($id) != null) {
                Category::whereId($id)->delete();
                return response()->json('Successfully deleted', 200);
            } else {
                return response()->json('Not found', 404);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
