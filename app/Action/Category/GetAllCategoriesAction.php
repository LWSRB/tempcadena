<?php

namespace App\Action\Category;

use App\Models\Category;

class GetAllCategoriesAction
{
    public function execute($request)
    {
        if (Category::count() == 0)
            return response()->json('Not found', 404);

        if ($request->has('limit'))
        {
            $data = Category::paginate($request->limit);
            return response()->json($data, 200);
        }

        $data = Category::all();
        if($data){
            return response()->json($data, 200);
        } else {
            return response()->json('Error', 400);
        }

    }
}
