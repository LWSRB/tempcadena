<?php

namespace App\Action\Category;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CreateCategoryAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $input = $request->all();

            $request->validate([
                'image' => 'mimes:jpg,png,jpeg|required|max:10000'
            ]);

            $image = $request->file('image');
            $imageName = time() . '-category-image' . '.' . $image->guessClientExtension();
            Storage::disk('category')->put($imageName, file_get_contents($image));

            if($request->has('sub_category_of'))
            {
                $category = Category::create([
                    'name' => $input['name'],
                    'description' => $input['description'],
                    'sub_category_of' => $input['sub_category_of'],
                    'image_path' => Storage::disk('category')->url($imageName),
                ]);

                if($category){
                    return response()->json($category, 200);
                } else {
                    return response()->json('Error', 400);
                }
            }

            $category = Category::create([
                'name' => $input['name'],
                'description' => $input['description'],
                'image_path' => Storage::disk('category')->url($imageName),
            ]);

            if($category){
                return response()->json($category, 200);
            } else {
                return response()->json('Error', 400);
            }

        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
