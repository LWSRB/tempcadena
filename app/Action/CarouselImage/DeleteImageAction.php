<?php

namespace App\Action\CarouselImage;

use App\Models\CarouselImage;
use Illuminate\Support\Facades\Auth;

class DeleteImageAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            if(CarouselImage::whereId($id)->count() === 0)
            {
                $res = 'No image with ID:'.$id.' found';
                return response()->json($res, 404);
            }

            if (CarouselImage::whereId($id)->count() !== 0)
            {
                CarouselImage::whereId($id)->delete();
                return response()->json(['success' => 'Successfully deleted'], 200);
            }

            return response()->json('error', 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}