<?php

namespace App\Action\CarouselImage;

use App\Models\CarouselImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CreateCarouselImageAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {

            $validated = $request->validate([
                'image' => 'mimes:jpg,png,jpeg|required|max:10000'
            ]);

            $image = $request->file('image');
            $imageName = time().'-carousel-image'.'.'.$image->guessClientExtension();
            $orderNumber = CarouselImage::all()->count() + 1;

            Storage::disk('carousel')->put($imageName, file_get_contents($image));

            $data = \DB::insert('INSERT into carousel_images (title, description, color, background, order_no, image_path, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $request->title, 
                $request->description, 
                $request->color, 
                $request->background, 
                $orderNumber, 
                env('APP_URL').Storage::disk('carousel')->url($imageName),
                now(),
                now()
            ]);

            // $data = CarouselImage::create([
            //     'title' => $request->title,
            //     'description' => $request->description,
            //     'color' => $request->color,
            //     'background' => $request->background,
            //     'order_no' => $orderNumber,
            //     'image_name' => $imageName,
            //     'image_path' => Storage::disk('carousel')->url($imageName)
            // ]);

            if ($data)
                return response()->json($data, 200);
            else
                return response()->json(['error' => 'Unsuccessfull'], 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
