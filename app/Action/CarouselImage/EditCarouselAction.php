<?php

namespace App\Action\CarouselImage;

use App\Models\CarouselImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EditCarouselAction
{
    public $success_status = 200;
    public function execute($request, $id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            if(CarouselImage::whereId($id)->count() === 0)
            {
                $response = 'No image found with ID:'.$id;
                return response()->json($response, 200);
            }

            $input = $request->all();
            $request->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
                'title' => 'required',
                'description' => 'required',
                'color' => 'required',
                'background' => 'required',
            ]);
    
            $replacedImage = CarouselImage::where('id', $id)->pluck('image_path')->all();
            $image = $request->file('image');
            $imageName = time() . '-carousel-image' . '.' . $request->file('image')->guessClientExtension();

            // unlink($replacedImage[0]);
            Storage::disk('carousel')->put($imageName, file_get_contents($image));

            $data = CarouselImage::whereId($id)->update([
                'image_path' => Storage::disk('carousel')->url($imageName),
                'title' => $input['title'],
                'description' => $input['description'],
                'color' => $input['color'],
                'background' => $input['background'],
                'order_no' => $input['order_no'],
            ]);

            if ($data)
                return response()->json($data, $this->success_status);
            else
                return response()->json(['error' => 'Unsuccessfull'], 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}