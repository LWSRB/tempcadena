<?php

namespace App\Action\ListingImages;

use App\Models\ListingsImage;

class GetListingImageByIdAction
{
    public function execute($id)
    {
        $record = ListingsImage::whereId($id)->get();
        if ($record == null)
            return response()->json('Not found', 404);
        else
            return response()->json($record, 200);
    }
}