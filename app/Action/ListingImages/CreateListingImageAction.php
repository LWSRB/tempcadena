<?php

namespace App\Action\ListingImages;

use App\Models\ListingsImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CreateListingImageAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' ||  Auth::user()->role == 'admin') {
            $input = $request->all();
            $request->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
                'listing_id' => 'required',
            ]);
    
            $image = $request->file('image');
            $imageName = time() . '-listing-image' . '.' . $request->file('image')->guessClientExtension();
            Storage::disk('listing')->put($imageName, file_get_contents($image));

            $data = ListingsImage::create([
                'image_path' => Storage::disk('listing')->url($imageName),
                'listing_id' => $input['listing_id'],
            ]);

            if ($data)
                return response()->json($data, 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}