<?php

namespace App\Action\ListingImages;

use App\Models\ListingsImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DeleteListingImageAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' ||  Auth::user()->role == 'admin') {
            if (ListingsImage::find($id) != null) {
                ListingsImage::whereId($id)->delete();
                return response()->json('Image Deleted', 200);
            } else {
                return response()->json('Error', 404);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
