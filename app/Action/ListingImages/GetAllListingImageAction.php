<?php

namespace App\Action\ListingImages;

use App\Models\ListingsImage;

class GetAllListingImageAction
{
    public function execute()
    {
        if (ListingsImage::count() == 0)
            return response()->json('Not found', 404);
        else
            return response()->json(ListingsImage::all(), 200);
    }
}
