<?php

namespace App\Action\CompareListing;

use App\Models\CompareListing;
use Illuminate\Support\Facades\Auth;

class RemoveComparisonAction
{
    public $success_status = 200;
    public function execute($request)
    {
        $user = Auth::id();
        $input = $request->all();
        $current = CompareListing::where(['user_id' => $user])->get();

        if (Auth::check() && Auth::user()->role == 'user') {

            if ($current->count() > 0) {
                if ($current[0]->listing_id_1 == $input['listing_id']) {
                    $data = CompareListing::where('user_id', $user)->update([
                        'listing_id_1' => null,
                    ])->get();
                    if ($data)
                        return response()->json($data, $this->success_status);
                    else
                        return response()->json('Error', 400);
                } else if ($current[0]->listing_id_2 == $input['listing_id']) {
                    $data = CompareListing::where('user_id', $user)->update([
                        'listing_id_2' => null,
                    ])->get();
                    if ($data)
                        return response()->json($data, $this->success_status);
                    else
                        return response()->json('Error', 400);
                } else if ($current[0]->listing_id_3 == $input['listing_id']) {
                    $data = CompareListing::where('user_id', $user)->update([
                        'listing_id_3' => null,
                    ])->get();
                    if ($data)
                        return response()->json($data, $this->success_status);
                    else
                        return response()->json('Error', 400);
                } else {
                    return response()->json(null, 400);
                }
            } else {
                CompareListing::where('user_id', $user)->delete();
                return response()->json('Compare listing deleted', $this->success_status);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
