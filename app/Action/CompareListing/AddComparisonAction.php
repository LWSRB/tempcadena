<?php

namespace App\Action\CompareListing;

use App\Models\CompareListing;
use Illuminate\Support\Facades\Auth;
use Nette\Utils\Json;

class AddComparisonAction
{
    public $success_status = 200;
    public function execute($request)
    {
        $user = Auth::id();
        $input = $request->all();
        $current = CompareListing::where(['user_id' => $user])->get();

        if (Auth::check() && Auth::user()->role == 'user') {
            if ($current->count() < 1) {
                $data = CompareListing::create([
                    'user_id' => $user,
                    'listing_id_1' => $input['listing_id'],
                    'listing_id_2' => null,
                    'listing_id_3' => null,
                ])->get();
                if ($data)
                    return response()->json($data, 200);
                else
                    return response()->json('Error', 400);
            } else {
                if ($current[0]->listing_id_1 == null) {
                    $data = CompareListing::where(['user_id' => $user])->update([
                        'listing_id_1' => $input['listing_id']
                    ])->get();
                    if ($data)
                        return response()->json($data, 200);
                    else
                        return response()->json('Error', 400);
                } else if ($current[0]->listing_id_2 == null) {
                    $data = CompareListing::where(['user_id' => $user])->update([
                        'listing_id_2' => $input['listing_id']
                    ])->get();
                    if ($data)
                        return response()->json($data, 200);
                    else
                        return response()->json('Error', 400);
                } else if ($current[0]->listing_id_3 == null) {
                    $data = CompareListing::where(['user_id' => $user])->update([
                        'listing_id_3' => $input['listing_id']
                    ])->get();
                    if ($data)
                        return response()->json($data, 200);
                    else
                        return response()->json('Error', 400);
                } else {
                    return response()->json(['error' => 'compare listing full'], 400);
                }
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
