<?php

namespace App\Action\Address;

use App\Models\Address;
use Illuminate\Support\Facades\Auth;

class DeleteAddressAction
{
    public function execute($id)
    {
        if (Auth::check()) {
            $data = Address::whereId($id)->delete();

            if ($data)
                return response()->json('Success', 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
