<?php

namespace App\Action\Address;

use App\Models\Address;
use Illuminate\Support\Facades\Auth;

class UpdateAddressAction
{
    public function execute($id, $request)
    {
        $request->validate([
            'address_1' => 'required',
            'address_2' => 'required',
            'city' => 'required',
            'province' => 'required',
            'region' => 'required',
            'postal_code' => 'required',
        ]);

        if (Auth::check()) {

            if(Address::whereId($id)->count() === 0) return response()->json('Address not found', 404);

            $input = $request->all();
            // $concatAddress = $input['address_1'].'+'.$input['address_2'].'+'.$input['city'].'+'.$input['province'].'+'.$input['region'];
            // $preparedAddress = str_replace(' ', '+', $concatAddress);
            // $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?new_forward_geocoder=true&address='.$preparedAddress.'&key='.$api_key);
            // $output = json_decode($geocode);
            // // dd($output);

            // $latitude = $output->results[0]->geometry->location->lat;
            // $longitude = $output->results[0]->geometry->location->lng;

            $latitude = 23.697809;
            $longitude = 120.960518;

            $data = Address::whereId($id)->update([
                'branch_name' => $input['branch_name'],
                'address_1' => $input['address_1'],
                'address_2' => $input['address_2'],
                'city' => $input['city'],
                'province' => $input['province'],
                'region' => $input['region'],
                'postal_code' => $input['postal_code'],
                'lat' => $latitude,
                'long' => $longitude
            ]);

            if ($data)
                return response()->json('Address updated', 200);
            else
                return response()->json('Error', 400);
        
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
