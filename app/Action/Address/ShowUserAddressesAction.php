<?php

namespace App\Action\Address;

use App\Models\Address;

class ShowUserAddressesAction
{
    public function execute($id)
    {
        $data = Address::where(['user_id' => $id])->get();

        if ($data)
            return response()->json($data, 200);
        else
            return response()->json('Error', 400);
    }
}
