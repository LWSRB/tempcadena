<?php

namespace App\Action\RegisteredDealer;

use App\Models\Distributor;
use App\Models\RegisteredDealer;
use Illuminate\Support\Facades\Auth;

class RequestRegistrationByDealerAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'dealer') {

            if(Distributor::whereId($request->distributor_id)->count() == 0) return response()->json('No distributor with ID#'.$request->distributor_id.' found.', 404);
            
            if(RegisteredDealer::where(['dealer_id' => Auth::id(), 'distributor_id' => $request->distributor_id])->count() > 0) return response()->json('prior request was sent', 200);

            $data = RegisteredDealer::create([
                'dealer_id' => Auth::id(),
                'distributor_id' => $request->distributor_id,
                'register_date' => now()
            ]);

            if ($data)
                return response()->json('Success', 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
