<?php

namespace App\Action\RegisteredDealer;

use App\Models\Distributor;
use App\Models\RegisteredDealer;
use App\Models\User;
use App\Notifications\RegistrationConfirmation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ApprovedByDistributorAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::user()->role === 'distributor' || Auth::user()->role === 'admin')
        {
            $input = $request->all();

            $data = RegisteredDealer::whereId($id)->update([
                'is_approved' => $input['is_approved']
            ]);

            if ($data){
                $placeholderId = RegisteredDealer::whereId($id)->get();
                $user = User::whereId($placeholderId[0]->dealer_id)->get();
                $distributor = Distributor::whereId($placeholderId[0]->distributor_id)->get();

                if($input['is_approved'] == 1){
                    $approvedNotification = [
                        'body' => 'Your request to be a dealer of '.$distributor[0]->name.' has been approved.',
                        'text' => 'Go to Cadena',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($user, new RegistrationConfirmation($approvedNotification));

                    return response()->json('Approved registration', 200);
                } else {
                    $declinedNotification = [
                        'body' => 'Your request to be a dealer of '.$distributor[0]->name.' has been declined.',
                        'text' => 'Submit another request',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($user, new RegistrationConfirmation($declinedNotification));

                    return response()->json('Registration declined', 200);
                }

            } else {
                return response()->json('Error', 400);
            }

        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

    }
}
