<?php

namespace App\Action\RegisteredDealer;

use App\Models\RegisteredDealer;
use Illuminate\Support\Facades\Auth;

class GetRegisteredDealerAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' || Auth::user()->role == 'admin') {

            if($request->has('distributor_id')){
                $record = RegisteredDealer::with('dealer')->where(['distributor_id' => $request->distributor_id])->get();
                if ($record->count() == 0) {
                    return response()->json(null, 204);
                } else {
                    return response()->json($record, 200);
                }
            }

            $record = RegisteredDealer::with('dealer')->get();

            if ($record->count() == 0) {
                return response()->json(null, 204);
            } else {
                return response()->json($record, 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}