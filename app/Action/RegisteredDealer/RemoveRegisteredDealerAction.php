<?php

namespace App\Action\RegisteredDealer;

use App\Models\RegisteredDealer;
use Illuminate\Support\Facades\Auth;

class RemoveRegisteredDealerAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role != 'user') {
            $data = RegisteredDealer::whereId($id)->get();

            if($data->count() == 0) return response()->json('Registered dealer id#'.$id.' not found.', 404);

            RegisteredDealer::whereId($id)->delete();
            return response()->json('Removed registered dealer', 200);
            
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}