<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CreateBrandAction
{
    public function execute($request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if (Auth::check() && Auth::user()->role === 'admin') {
            $input = $request->all();
            $request->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            $image = $request->file('image');
            $imageName = time() . '-brand-image' . '.' . $image->guessClientExtension();
            Storage::disk('brand')->put($imageName, file_get_contents($image));
            $path = Storage::disk('brand')->url($imageName);

            $data = Brand::create([
                'name' => $input['name'],
                'description' => $input['description'],
                'image_path' => $path
            ]);
            return response()->json($data, 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}