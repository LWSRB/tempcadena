<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ApproveBrandAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $input = $request->all();

            $data = Brand::whereId($id)->update([
                'is_approved' => $input['approve']
            ]);

            if ($data) {
                if($input['approve'] == 1) return response()->json('Brand approved', 200);
                if($input['approve'] == 0) return response()->json('Brand entry not approved', 200);
            } else {
                return response()->json('Not found', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
