<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;

class SearchBrandAction
{
    public function execute($id, $request)
    {
        $input = $request->all();

        if($request->has('approved')){

            $record = Brand::where([
                'id' => $id,
                'is_approved' => $input['approved']
            ])->get();
    
            if ($record == null)
                return response()->json('Not found', 404);
            else
                return response()->json($record, 200);
        } 

        $record = Brand::whereId($id);
        return response()->json($record, 200);
    }
}
