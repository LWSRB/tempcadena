<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;

class GetAllBrandAction
{
    public function execute($request)
    {
        $input = $request->all();

        if(Brand::count() <= 0) return response()->json('No brand/s found', 200);

        if($request->has('limit'))
        {
            if($request->has('approved'))
            {
                $data = Brand::where(['is_approved' => $input['approved']])->paginate($request->limit);
                if($data){
                    return response()->json($data, 200);
                } else {
                    return response()->json('Error', 400);
                }
            }
            $data = Brand::paginate($request->limit);
            return response()->json($data, 200);
        }

        if($request->has('approved')){
            $data = Brand::where(['is_approved' => $input['approved']])->get();

            if($data){
                return response()->json($data, 200);
            } else {
                return response()->json('Error', 400);
            }
        }

        $data = Brand::all();
        return response()->json($data, 200);
    }
}
