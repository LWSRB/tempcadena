<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UpdateBrandAction
{
    public $success_status = 200;
    public function execute($request, $id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $input = $request->all();
            $request->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            // $replacedImage = Brand::where('id', $id)->pluck('image-path')->all();
            $image = $request->file('image');
            $imageName = time() . '-brand-image' . '.' . $image->guessClientExtension();
            // unlink($replacedImage[0]);
            $pathName = Storage::disk('brand')->put($imageName, file_get_contents($image));

            $record = Brand::whereId($id)->update([
                'name' => $input['name'],
                'description' => $input['description'],
                'image_path' => Storage::disk('brand')->url($imageName),
            ]);

            if ($record)
                return response()->json('Brand entry edited', $this->success_status);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}