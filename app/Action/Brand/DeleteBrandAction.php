<?php

namespace App\Action\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DeleteBrandAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            if (Brand::find($id) != null) {
                Brand::whereId($id)->delete();
                return response()->json('Successfully deleted', 200);
            } else {
                return response()->json('Not found', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}