<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use App\Models\Listing;
use App\Models\User;
use App\Notifications\UserInquiry;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CreateInquiryAction
{
    public function execute($request)
    {
        $request->validate([
            'dealer_id' => 'required',
            'email' => 'email|required',
            'full_name' => 'required',
            'contact_number' => 'required',
            'address' => 'required',
            'message' => 'required',
        ]);

        if (Auth::check()) {
            if (Listing::whereId($request->listing_id)->count() <=  0) return response()->json('No listing with ID#'.$request->listing_id.' found.', 404);

            // $data = Inquiry::create([
            //     'user_id' => Auth::id(),
            //     'dealer_id' => $request->dealer_id,
            //     'listing_id' => $request->listing_id,
            //     'full_name' => $request->full_name,
            //     'email' => $request->email,
            //     'contact_number' => $request->contact_number,
            //     'address' => $request->address,
            //     'messsage' => $request->messsage
            // ]);

            $data = \DB::insert('INSERT into inquiries (user_id, dealer_id, listing_id, full_name, email, contact_number, address, message) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', [Auth::id(), $request->dealer_id, $request->listing_id, $request->full_name, $request->email, $request->contact_number, $request->address, $request->message]);

            $receiver = User::whereId($request->dealer_id)->get();

            $inquiryNotification = [
                'body' => $request->full_name.' sent you a message.',
                'text' => 'Go to Cadena',
                'url' => 'www.cadena.com',
                'extra' => 'click the button above to check the message'
            ];

            Notification::send($receiver, new UserInquiry($inquiryNotification));

            return response()->json('Inquiry sent', 200);
        } else {
            return response()->json('Unauthenticated', 401);
        }
    }
}