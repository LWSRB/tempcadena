<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use App\Models\Listing;
use Illuminate\Support\Facades\Auth;

class AddRemarksAction
{
    public function execute($id, $request)
    {
        $input = $request->all();
        if(Auth::check() && Auth::id() == $input['dealer_id']){
            $data = Inquiry::whereId($id)->update([
                'remarks' => $input['remarks']
            ]);

            if($data)
                return response()->json('Success', 200);
                else
                return response()->json('Error', 400);
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
