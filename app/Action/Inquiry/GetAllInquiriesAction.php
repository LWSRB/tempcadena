<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use Illuminate\Support\Facades\Auth;

class GetAllInquiriesAction
{
    public function execute()
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            if (Inquiry::count() == 0) {
                return response()->json('Not found', 404);
            } else {
                $data = Inquiry::all();

                if ($data)
                    return response()->json($data, 200);
                else
                    return response()->json('Error', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}