<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use Illuminate\Support\Facades\Auth;

class GetInquiryByIdAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role != 'user') {
            if (Inquiry::count() == 0) {
                return response()->json('Not found', 404);
            } else {
                $data = Inquiry::where(['id' => $id, 'dealer_id' => Auth::id()])->get();

                if ($data)
                    return response()->json($data, 200);
                else
                    return response()->json('Error', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
