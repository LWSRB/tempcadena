<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use Illuminate\Support\Facades\Auth;

class GetUserInquiryAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::id() == $id) {
            if (Inquiry::count() == 0) {
                return response()->json('Not found', 404);
            } else {
                $data = Inquiry::where(['dealer_id' => $id])->get();

                if ($data)
                    return response()->json($data, 200);
                else
                    return response()->json('Error', 400);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
