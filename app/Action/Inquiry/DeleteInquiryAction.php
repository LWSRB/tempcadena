<?php

namespace App\Action\Inquiry;

use App\Models\Inquiry;
use Illuminate\Support\Facades\Auth;

class DeleteInquiryAction
{
    public function execute($id)
    {
        $inquiry = Inquiry::whereId($id)->get();

        if (Auth::check() && Auth::id() == $inquiry[0]->dealer_id) {

            if (Inquiry::whereId($id) != null) {
                Inquiry::whereId($id)->delete();
                return response()->json(['success' => 'deleted'], 200);
            } else {
                return response()->json(['error' => 'error'], 404);
            }
        } else {
            return response()->json('Unauthorized', 401);
        }
    }
}
