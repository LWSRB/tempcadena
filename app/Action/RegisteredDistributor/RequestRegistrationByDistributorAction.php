<?php

namespace App\Action\RegisteredDistributor;

use App\Models\Distributor;
use App\Models\RegisteredDistributor;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RequestRegistrationByDistributorAction
{
    public function execute($request)
    {
        if (Auth::check() && Auth::user()->role == 'distributor' || Auth::user()->role === 'admin') {

            if(Distributor::whereId($request->distributor_id)->count() == 0) return response()->json('No distributor with ID#'.$request->distributor_id.' found.', 404);
            if(User::where(['id' => $request->dealer_id, 'role' => 'dealer'])->count() == 0) return response()->json('No dealer with ID#'.$request->dealer_id.' found.', 404);
            if(RegisteredDistributor::where(['dealer_id' => $request->dealer_id, 'distributor_id' => $request->distributor_id])->count() > 0) return response()->json('prior request was sent', 200);

            $data = RegisteredDistributor::create([
                'dealer_id' => $request->dealer_id,
                'distributor_id' => $request->distributor_id,
                'register_date' => now()
            ]);

            if ($data)
                return response()->json('Success', 200);
            else
                return response()->json('Error', 400);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
