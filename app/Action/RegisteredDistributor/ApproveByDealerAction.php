<?php

namespace App\Action\RegisteredDistributor;

use App\Models\RegisteredDistributor;
use App\Models\User;
use App\Models\Distributor;
use App\Notifications\RegistrationConfirmation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ApproveByDealerAction
{
    public function execute($id, $request)
    {
        if (Auth::check() && Auth::user()->role == 'dealer') {
            $data = RegisteredDistributor::where(['id' => $id, 'dealer_id' => Auth::id()])->update([
                'is_approved' => $request->is_approved
            ]);

            if ($data){
                $placeholderId = RegisteredDistributor::whereId($id)->get('distributor_id');
                $distributor = Distributor::whereId($placeholderId[0]->distributor_id)->get();

                if($request->is_approved == 1){
                    $approvedNotification = [
                        'body' => 'Your request to be a distributor of '.Auth::user()->name.' has been approved.',
                        'text' => 'Go to Cadena',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($distributor, new RegistrationConfirmation($approvedNotification));

                    return response()->json('Approved registration', 200);
                } else {
                    $declinedNotification = [
                        'body' => 'Your request to be a distributor of '.Auth::user()->name.' has been declined.',
                        'text' => 'Submit another request',
                        'url' => 'www.cadena.com',
                        'extra' => 'click the button above to view your account'
                    ];

                    Notification::send($distributor, new RegistrationConfirmation($declinedNotification));

                    return response()->json('Registration declined', 200);
                }

            } else {
                return response()->json('Error', 400);
            }

        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
