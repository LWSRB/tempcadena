<?php

namespace App\Action\RegisteredDistributor;

use App\Models\RegisteredDistributor;
use Illuminate\Support\Facades\Auth;

class RemoveRegisteredDistributorAction
{
    public function execute($id)
    {
        if (Auth::check() && Auth::user()->role != 'user') {
            $data = RegisteredDistributor::whereId($id)->get();

            if($data->count() == 0) return response()->json('Registered dealer id#'.$id.' not found.', 404);

            if ($data == null) {
                return response()->json(null, 204);
            } else {
                RegisteredDistributor::find($id)->delete();
                return response()->json('Removed registered distributor', 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}