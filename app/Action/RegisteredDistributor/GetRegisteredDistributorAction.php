<?php

namespace App\Action\RegisteredDistributor;

use App\Models\RegisteredDistributor;
use Illuminate\Support\Facades\Auth;

class GetRegisteredDistributorAction
{
    public function execute()
    {
        $dealer = Auth::id();
        if (Auth::check() && Auth::user()->role == 'dealer') {
            $record = RegisteredDistributor::with('distributor')->where(['dealer_id' => $dealer])->get();

            if ($record->count() == 0) {
                return response()->json(null, 204);
            } else {
                return response()->json($record, 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
