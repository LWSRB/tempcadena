<?php

use App\Http\Controllers\AddressesController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CarouselImagesController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CompareListingsController;
use App\Http\Controllers\DealerListingsController;
use App\Http\Controllers\DistributorsController;
use App\Http\Controllers\InquiriesController;
use App\Http\Controllers\ListingsController;
use App\Http\Controllers\ListingsImageController;
use App\Http\Controllers\RegisteredDealersController;
use App\Http\Controllers\RegisteredDistributorsController;
use App\Http\Controllers\UserListingController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\WishlistsController;
use Illuminate\Support\Facades\Route;


//GUEST

//USER
Route::post('/user/register', [UsersController::class, 'RegisterUser']); 
Route::post('/user/login', [UsersController::class, 'UserLogin']);

Route::get('/user/login/google');
Route::get('/user/logout', [UsersController::class, 'UserLogout']);
Route::get('/user/dealers', [UsersController::class, 'GetDealersList']);
Route::get('/user/distributors', [UsersController::class, 'GetDistributorsList']);

Route::get('/brand', [BrandsController::class, 'getBrand']);
Route::get('/brand/{id}', [BrandsController::class, 'searchBrand']);

Route::get('/listing', [ListingsController::class, 'getAllListing']);
Route::get('/listing/{id}', [ListingsController::class, 'searchListing']);
Route::get('/search', [ListingsController::class, 'search']);

Route::get('/category', [CategoriesController::class, 'getCategory']);
Route::get('/category/{id}', [CategoriesController::class, 'searchCategory']);

Route::get('/carousel-img', [CarouselImagesController::class, 'getAllCarouselImages']);
Route::get('/carousel-img/{id}', [CarouselImagesController::class, 'searchCarouselImage']);

Route::get('/listing-img', [ListingsImageController::class, 'getAllImages']);
Route::get('/listing-img/{id}', [ListingsImageController::class, 'searchListingImage']);

Route::get('/promo', [UserListingController::class, 'getAllUserListing']);
Route::get('/promo/{id}', [UserListingController::class, 'searchUserListing']);

Route::get('/distributor', [DistributorsController::class, 'getDistributor']);

Route::get('/user/{user_id}/address', [AddressesController::class, 'show']);

Route::get('/dealer-listing', [DealerListingsController::class, 'get']); //bug unable to link listing

//BASIC API AUTH ONLY
Route::middleware('auth:api')->group(function () {
    //USER
    Route::post('/user/{id}/update', [UsersController::class, 'EditUserDetails']);
    Route::get('/user', [UsersController::class, 'UserDetails']);
    Route::post('/user/{id}/password', [UsersController::class, 'ChangePassword']);

    //ADDRESSES
    Route::post('/user/address', [AddressesController::class, 'create']);
    Route::post('/user/address/{id}/update', [AddressesController::class, 'update']);
    Route::delete('/user/address/{address_id}/delete', [AddressesController::class, 'destroy']);


    //INQUIRY
    Route::post('/inquiry', [InquiriesController::class, 'newInquiry']);

    //WISHLIST
    Route::post('/wishlist', [WishlistsController::class, 'addWishlist']);
    Route::get('/wishlist', [WishlistsController::class, 'getWishlist']); //bug unable to link listing
    Route::delete('/wishlist/{id}', [WishlistsController::class, 'removeWishlist']);

    //COMPARE LISTINGS
    Route::post('/compare/add', [CompareListingsController::class, 'addComparison']);
    Route::post('/compare/remove', [CompareListingsController::class, 'removeComparison']);
});



Route::middleware('auth:api')->group(function () {

    //USER LISTING
    Route::post('/promo', [UserListingController::class, 'newUserListing']);
    Route::post('/promo/{id}', [UserListingController::class, 'editUserListing']);
    Route::delete('/promo/{id}', [UserListingController::class, 'deleteUserListing']);
    Route::post('/promo/{id}/status', [UserListingController::class, 'userListingActivation']);

    //LISTING IMAGES (DISTRIBUTOR AND ADMIN)
    Route::post('/listing-img', [ListingsImageController::class, 'newListingImage']);
    Route::delete('/listing-img/{id}', [ListingsImageController::class, 'deleteListingImage']);

    //INQUIRY
    Route::get('/inquiry/{id}', [InquiriesController::class, 'getUserInquiries']);
    Route::delete('/inquiry/{id}/delete', [InquiriesController::class, 'deleteInquiry']);
    Route::get('/inquiry/get/{id}', [InquiriesController::class, 'getInquiryById']);
    Route::post('/inquiry/remarks/{id}', [InquiriesController::class, 'addRemarks']);

});



Route::middleware('auth:api')->group(function () {
    Route::post('/registered-dealer', [RegisteredDealersController::class, 'addDealer']);
    Route::get('/registered-dealer', [RegisteredDealersController::class, 'getRegisteredDealer']);
    Route::delete('/registered-dealer/{id}', [RegisteredDealersController::class, 'removeDealer']);
    Route::post('/registered-dealer/{id}/request', [RegisteredDealersController::class, 'approveDealerRequest']);

    Route::post('/registered-distributor', [RegisteredDistributorsController::class, 'requestAddDistributor']);
    Route::get('/registered-distributor', [RegisteredDistributorsController::class, 'getRegisteredDistributors']);
    Route::delete('/registered-distributor/{id}', [RegisteredDistributorsController::class, 'removeRegisteredDistributor']);
    Route::post('/registered-distributor/{id}/request', [RegisteredDistributorsController::class, 'approveDistributorRequest']);

    //DEALERLISTING
    Route::post('/dealer-listing', [DealerListingsController::class, 'add']);
    Route::delete('/dealer-listing/{id}', [DealerListingsController::class, 'delete']);
    Route::post('/dealer-listing/{id}/active', [DealerListingsController::class, 'activate']);
});


//ADMIN ROLES ONLY
Route::middleware('auth:api')->group(function () {
    //LISTING
    Route::post('/listing/{id}/approve', [ListingsController::class, 'approve']);
    Route::post('/listing', [ListingsController::class, 'newListing']);
    Route::post('/listing/{id}', [ListingsController::class, 'editListing']);
    Route::delete('/listing/{id}', [ListingsController::class, 'deleteListing']);
    Route::post('/listing/{id}/activate', [ListingsController::class, 'activate']);
    Route::post('/listing/{id}/privacy', [ListingsController::class, 'changePrivacy']);


    Route::get('/admin/get-inquiries', [InquiriesController::class, 'getAllInquiries']);

    Route::post('/dealer-listing/{id}/approve', [DealerListingsController::class, 'approve']);

    //BRAND
    Route::delete('/brand/{id}', [BrandsController::class, 'deleteBrand']);
    Route::post('/brand/{id}/approve', [BrandsController::class, 'approveBrand']);
    Route::post('/brand', [BrandsController::class, 'newBrand']);
    Route::post('/brand/{id}', [BrandsController::class, 'editBrand']);

    //CATEGORY
    Route::post('/category', [CategoriesController::class, 'newCategory']);
    Route::post('/category/{category}', [CategoriesController::class, 'editCategory']);
    Route::delete('/category/{category}', [CategoriesController::class, 'deleteCategory']);

    //CAROUSEL IMAGES
    Route::post('/carousel-img', [CarouselImagesController::class, 'newCarouselImage']);
    Route::delete('/carousel-img/{id}', [CarouselImagesController::class, 'deleteCarouselImage']);
    Route::post('/carousel-img/{id}', [CarouselImagesController::class, 'editCarouselImage']);

    //USERS
    Route::post('/user/{id}/role', [UsersController::class, 'UpdateUserRole']);

    //DISTRIBUTOR
    Route::post('/distributor/new', [DistributorsController::class, 'addDistributor']);
    Route::post('/distributor/{id}/update', [DistributorsController::class, 'editDistributor']);
    Route::delete('/distributor/{id}/remove', [DistributorsController::class, 'deleteDistributor']);
});
