<?php

use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('/home', 'home')->middleware('auth'); //->middleware(['auth', 'verified']);

Route::get('/user/{driver}', [UsersController::class, 'ProviderLogin'])->name('user.provider.login');

Route::get('/user/login/facebook', [UsersController::class, 'FacebookLogin']);
Route::get('/user/login/facebook/redirect', [UsersController::class, 'FacebookRedirect']);
