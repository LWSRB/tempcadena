<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Category::factory(10)->create();
        \App\Models\Listing::factory(10)->create();
        // \App\Models\ListingsImage::factory(10)->create();
        \App\Models\Brand::factory(10)->create();
        \App\Models\UserListing::factory(10)->create();
        \App\Models\Inquiry::factory(10)->create();
    }
}
