<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('distributor_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->integer('sub_category_id')->nullable(); //pending (for future use)
            $table->unsignedInteger('brand_id')->nullable();

            $table->string('series')->nullable();
            $table->string('product_model')->nullable();
            $table->string('color')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price_from')->nullable();
            $table->decimal('price_to')->nullable();
            $table->string('warranty')->nullable();
            $table->integer('no_of_views')->default(0);
            $table->boolean('is_approved')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_public')->default(1);
            $table->text('remarks')->nullable();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('distributor_id')->references('id')->on('distributors')->onDelete('CASCADE');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('SET NULL');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}