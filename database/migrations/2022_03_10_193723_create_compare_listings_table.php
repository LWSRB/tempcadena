<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompareListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compare_listings', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('listing_id_1')->nullable();
            $table->unsignedInteger('listing_id_2')->nullable();
            $table->unsignedInteger('listing_id_3')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('listing_id_1')->references('id')->on('listings')->onDelete(null);
            $table->foreign('listing_id_2')->references('id')->on('listings')->onDelete(null);
            $table->foreign('listing_id_3')->references('id')->on('listings')->onDelete(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compare_listings');
    }
}
