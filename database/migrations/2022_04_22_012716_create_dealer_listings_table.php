<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealerListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_listings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dealer_id');
            $table->unsignedInteger('listing_id');
            $table->boolean('is_approved')->default(0);
            $table->boolean('is_active')->default(0);
            $table->timestamps();

            $table->foreign('dealer_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_listings');
    }
}
