<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisteredDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dealer_id')->nullable(); 
            $table->unsignedInteger('distributor_id')->nullable(); 
            $table->timestamp('register_date')->useCurrent();
            $table->boolean('is_approved')->default(0);
            $table->timestamps();

            $table->foreign('distributor_id')->references('id')->on('distributors')->onDelete('SET NULL');
            $table->foreign('dealer_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_dealers');
    }
}