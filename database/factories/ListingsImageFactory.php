<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class ListingsImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $listing = DB::table('listings')->pluck('id')->get(rand(0, 9));
        return [
            'listing_id' => $listing,
            'image' => $this->faker->asciify('image link'),
        ];
    }
}
