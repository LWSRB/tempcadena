<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class InquiryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $listing = DB::table('listings')->pluck('id')->get(rand(0,9));
        $user = DB::table('users')->pluck('id')->get(rand(0,9));
        return [
            'user_id' => $user,
            'listing_id' => $listing,
            'full_name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'contact_number' => $this->faker->phoneNumber(),
            'address' => $this->faker->address(),
            'messsage' => $this->faker->paragraph(),
        ];
    }
}
