<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

// use App\Models\Listing;
// use Faker\Generator as Faker;


class ListingFactory extends Factory
// class ListingFactory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dealer_id = DB::table('users')->pluck('id')->get(rand(1, 9));
        $category_id = DB::table('categories')->pluck('id')->get(rand(1, 9));

        return [
            'dealer_id' => $dealer_id,
            'category_id' => $category_id,
            'brand_id' => $this->faker->numerify('#'),
            'sub_category_id' => $this->faker->numerify('#'),
            'sub_category_2_id' => $this->faker->numerify('#'),
            'product_model' => $this->faker->asciify('****'),
            'price_from' => $this->faker->numerify('#0000'),
            'price_to' => $this->faker->numerify('#00000'),
            'description' => $this->faker->paragraph(),
            'series' => $this->faker->text(),
            'type' => $this->faker->text(),
            'color' => $this->faker->colorName(),
            'product_fit' => $this->faker->text(),
            'quantity_sold' => $this->faker->numerify('##') ,
            'light_source' => $this->faker->name(),
            'recommended_use' => $this->faker->text(),
            'assembly' => $this->faker->text(),
            'anticipated_ship_out_time' => $this->faker->date(),
            'certification' => $this->faker->text(),
            'replaces_oe_number' => $this->faker->asciify('****'),
            'replaces_partslink_number' => $this->faker->asciify('****'),
            'warranty' => $this->faker->text(),
        ];
    }
}
