<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->asciify('bike'),
            'description' => $this->faker->paragraph(),
            'parent_id'=> $this->faker->numerify('#'),
            'image' => $this->faker->asciify('image link')
        ];
    }
}
