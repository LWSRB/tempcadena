<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class BrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $brand = ['honda', 'toyota', 'nissan', 'mitsubishi'];
        $distributor = DB::table('users')->pluck('id')->get(rand(1, 9));
        $listing = DB::table('listings')->pluck('id')->get(rand(1, 9));
        return [
            'name' => $brand[rand(0, 3)],
            'description' => $this->faker->paragraph(),
            'image-path' => $this->faker->asciify('image link'),
            'distributor_id' => $distributor,
            'listing_id' => $listing,
        ];
    }
}
