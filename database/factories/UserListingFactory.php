<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class UserListingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $listing = DB::table('listings')->pluck('id')->get(rand(0,9));
        $user = DB::table('users')->pluck('id')->get(rand(0,9));
        $status = ['active', 'not active'];
        $promo = ['discount', 'content', 'event'];
        return [
            'listing_id' => $listing,
            'user_id' => $user,
            'description' => $this->faker->paragraph(),
            'status' => $status[rand(0,1)],
            'featured_expiration' => $this->faker->date('Y_m_d'),
            'discount' => $this->faker->numerify('#000'),
            'price' => $this->faker->numerify('#0000'),
            'promo_type' => $promo[rand(0,2)],
            'promo_expiration' => $this->faker->date('Y_m_d'),
        ];
    }
}
